package orders

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"golang-task6/internal/models"
	"golang-task6/internal/store"
	"log"
	"net/http"

	"github.com/gofrs/uuid"
	"github.com/gorilla/mux"
)

// GetOrders godoc
// @Tags Orders
// @Summary Returns list of orders
// @Description Shows all orders
// @ID get-orders
// @Produce	json
// @Success 200 {object} []models.Order
// @Failure 400,404 {object} string
// @Failure 500,503 {object} string
// @Failure default {object} string
// @Router /orders [get]
func (o Orders) GetOrders(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		orders := store.Orders{}
		data, err := orders.GetOrders(db)
		if err != nil {
			if err == sql.ErrNoRows {
				fmt.Println(err)
				http.Error(w, err.Error(), http.StatusNotFound)
				return
			}
			fmt.Println(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(data)
	}
}

// GetOrder godoc
// @Tags Orders
// @Summary Returns order
// @Description Shows an order based on provided UUID
// @ID get-order
// @Produce	json
// @Param uuid path string true "Order UUID"
// @Success 200 {object} models.Order
// @Failure 400,404 {object} string
// @Failure 500,503 {object} string
// @Failure default {object} string
// @Router /orders/{uuid} [get]
func (o Orders) GetOrder(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		order := models.Order{}

		params := mux.Vars(r)
		UUID, err := uuid.FromString(params["uuid"])
		if err != nil {
			log.Println(err)
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		if UUID == uuid.Nil {
			w.WriteHeader(http.StatusForbidden)
			return
		}

		order.UUID = UUID
		orders := store.Orders{}
		err = orders.GetOrder(db, &order)
		if err != nil {
			if err == sql.ErrNoRows {
				log.Println(err)
				http.Error(w, err.Error(), http.StatusNotFound)
				return
			}
			log.Println(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(order)

	}
}

// AddOrder godoc
// @Tags Orders
// @Summary Creates Order
// @Description Create new order based on given book UUID and description
// @ID create-order
// @Accept	json
// @Produce	json
// @Param order body models.Order true "New order book UUID and description"
// @Success 200 {object} models.Order
// @Failure 400,404 {object} string
// @Failure 500,503 {object} string
// @Failure default {object} string
// @Router /orders [post]
func (o Orders) AddOrder(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		order := models.Order{}

		err := json.NewDecoder(r.Body).Decode(&order)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			log.Println("JSON Decode error:", err)
			return
		}
		if order.BookUUID == uuid.Nil {
			log.Println("Book UUID is not provided")
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		orders := store.Orders{}
		err = orders.AddOrder(db, &order)
		if err != nil {
			log.Println(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(order)
	}
}

// UpdateOrder godoc
// @Tags Orders
// @Summary Updates order
// @Description Updates an order based on new book UUID and description
// @ID update-order
// @Accept	json
// @Produce	json
// @Param uuid path string true "Order UUID"
// @Param order body models.Order true "Order book UUID and description"
// @Success 200 {object} models.Order
// @Failure 400,404 {object} string
// @Failure 500,503 {object} string
// @Failure default {object} string
// @Router /orders/{uuid} [put]
func (o Orders) UpdateOrder(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		order := models.Order{}

		params := mux.Vars(r)
		UUID, err := uuid.FromString(params["uuid"])
		if err != nil {
			log.Println(err)
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		err = json.NewDecoder(r.Body).Decode(&order)
		if err != nil {
			log.Println("JSON Decode error:", err)
			http.Error(w, err.Error(), http.StatusBadRequest)
		}

		if order.BookUUID == uuid.Nil {
			log.Println("Book UUID is not provided")
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		order.UUID = UUID
		orders := store.Orders{}
		err = orders.UpdateOrder(db, &order)
		if err != nil {
			log.Println(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(order)
	}
}

// DeleteOrder godoc
// @Tags Orders
// @Summary Deletes order
// @Description Deletes an order based on UUID
// @ID delete-order
// @Produce	json
// @Param uuid path string true "Order UUID"
// @Success 200
// @Failure 400,404 {object} string
// @Failure 500,503 {object} string
// @Failure default {object} string
// @Router /orders/{uuid} [delete]
func (o Orders) DeleteOrder(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		order := models.Order{}
		params := mux.Vars(r)

		UUID, err := uuid.FromString(params["uuid"])
		if err != nil {
			log.Println(err)
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		order.UUID = UUID
		orders := store.Orders{}
		err = orders.GetOrder(db, &order)
		if err != nil {
			if err == sql.ErrNoRows {
				http.Error(w, err.Error(), http.StatusNotFound)
				return
			}
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		err = orders.DeleteOrder(db, &order)
		if err != nil {
			log.Println(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.WriteHeader(http.StatusOK)
	}
}
