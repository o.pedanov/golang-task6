package orders

import (
	"context"
	"encoding/json"
	"fmt"
	"golang-task6/internal/models"
	pb "golang-task6/internal/proto"
	"log"
	"net/http"

	"github.com/gofrs/uuid"
	"github.com/gorilla/mux"
)

// GetAuthors godoc
// @Tags Authors
// @Summary Returns list of authors
// @Description Shows all authors
// @ID get-authors
// @Produce	json
// @Success 200 {object} []models.Author
// @Failure 400,404 {object} string
// @Failure 500,503 {object} string
// @Failure default {object} string
// @Router /authors [get]
func (o Orders) GetAuthors(ctx context.Context, g pb.CatalogClient) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		request := &pb.Empty{}
		response, err := g.GetAuthors(ctx, request)
		if err != nil {
			fmt.Println("gRPC error:", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}

		var authors []models.Author

		err = json.Unmarshal(response.Authors, &authors)
		if err != nil {
			log.Println("authors unmarshal error:", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		json.NewEncoder(w).Encode(authors)
		w.WriteHeader(http.StatusOK)
	}
}

// GetAuthor godoc
// @Tags Authors
// @Summary Returns author
// @Description Shows an author based on UUID
// @ID get-author
// @Produce	json
// @Param uuid path string true "Author UUID"
// @Success 200 {object} models.Author
// @Failure 400,404 {object} string
// @Failure 500,503 {object} string
// @Failure default {object} string
// @Router /authors/{uuid} [get]
func (o Orders) GetAuthor(ctx context.Context, g pb.CatalogClient) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		params := mux.Vars(r)
		UUID, err := uuid.FromString(params["uuid"])
		if err != nil {
			log.Println(err)
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		if UUID == uuid.Nil {
			w.WriteHeader(http.StatusForbidden)
			return
		}

		request := &pb.UUID{
			Uuid: params["uuid"],
		}

		response, err := g.GetAuthor(ctx, request)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			log.Println("gRPC error:", err)
			return
		}

		err = json.Unmarshal(response.Author, &o.author)
		if err != nil {
			log.Println("author unmarshal error:", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		json.NewEncoder(w).Encode(o.author)
		w.WriteHeader(http.StatusOK)
	}
}

// GetBooks godoc
// @Tags Books
// @Summary Returns list of books
// @Description Shows all books
// @ID get-books
// @Produce	json
// @Success 200 {object} []models.Book
// @Failure 400,404 {object} string
// @Failure 500,503 {object} string
// @Failure default {object} string
// @Router /books [get]
func (o Orders) GetBooks(ctx context.Context, g pb.CatalogClient) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		request := &pb.Empty{}
		response, err := g.GetBooks(ctx, request)
		if err != nil {
			fmt.Println("gRPC error:", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}

		var books []models.Book

		err = json.Unmarshal(response.Books, &books)
		if err != nil {
			log.Println("books unmarshal error:", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		json.NewEncoder(w).Encode(books)
		w.WriteHeader(http.StatusOK)
	}
}

// GetBook godoc
// @Tags Books
// @Summary Returns book
// @Description Shows a book based on UUID
// @ID get-book
// @Produce	json
// @Param uuid path string true "Book UUID"
// @Success 200 {object} models.Book
// @Failure 400,404 {object} string
// @Failure 500,503 {object} string
// @Failure default {object} string
// @Router /books/{uuid} [get]
func (o Orders) GetBook(ctx context.Context, g pb.CatalogClient) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		params := mux.Vars(r)
		UUID, err := uuid.FromString(params["uuid"])
		if err != nil {
			log.Println(err)
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		if UUID == uuid.Nil {
			w.WriteHeader(http.StatusForbidden)
			return
		}

		request := &pb.UUID{
			Uuid: params["uuid"],
		}

		response, err := g.GetBook(ctx, request)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			log.Println("gRPC error:", err)
			return
		}

		err = json.Unmarshal(response.Book, &o.book)
		if err != nil {
			log.Println("author unmarshal error:", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		json.NewEncoder(w).Encode(o.book)
		w.WriteHeader(http.StatusOK)
	}
}

// GetCategories godoc
// @Tags Categories
// @Summary Returns list of categories
// @Description Shows all books categories
// @ID get-categories
// @Produce	json
// @Success 200 {object} []models.Category
// @Failure 400,404 {object} string
// @Failure 500,503 {object} string
// @Failure default {object} string
// @Router /categories [get]
func (o Orders) GetCategories(ctx context.Context, g pb.CatalogClient) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		request := &pb.Empty{}
		response, err := g.GetCategories(ctx, request)
		if err != nil {
			fmt.Println("gRPC error:", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}

		var categories []models.Category

		err = json.Unmarshal(response.Categories, &categories)
		if err != nil {
			log.Println("authors unmarshal error:", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		json.NewEncoder(w).Encode(categories)
		w.WriteHeader(http.StatusOK)
	}
}

// GetCategory godoc
// @Tags Categories
// @Summary Returns category
// @Description Shows a books category based on UUID
// @ID get-category
// @Produce	json
// @Param uuid path string true "Category UUID"
// @Success 200 {object} models.Category
// @Failure 400,404 {object} string
// @Failure 500,503 {object} string
// @Failure default {object} string
// @Router /categories/{uuid} [get]
func (o Orders) GetCategory(ctx context.Context, g pb.CatalogClient) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		params := mux.Vars(r)
		UUID, err := uuid.FromString(params["uuid"])
		if err != nil {
			log.Println(err)
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		if UUID == uuid.Nil {
			w.WriteHeader(http.StatusForbidden)
			return
		}

		request := &pb.UUID{
			Uuid: params["uuid"],
		}

		response, err := g.GetCategory(ctx, request)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			log.Println("gRPC error:", err)
			return
		}

		err = json.Unmarshal(response.Category, &o.category)
		if err != nil {
			log.Println("author unmarshal error:", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		json.NewEncoder(w).Encode(o.category)
		w.WriteHeader(http.StatusOK)
	}
}

// GetBooksByAuthor godoc
// @Tags Filters
// @Summary Returns list of books by author
// @Description Shows all books written by specified author with provided UUID
// @ID get-books-by-author
// @Produce	json
// @Param uuid path string true "Author UUID"
// @Success 200 {object} []models.Book
// @Failure 400,404 {object} string
// @Failure 500,503 {object} string
// @Failure default {object} string
// @Router /books/author/{uuid} [get]
func (o Orders) GetBooksByAuthor(ctx context.Context, g pb.CatalogClient) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		params := mux.Vars(r)
		UUID, err := uuid.FromString(params["uuid"])
		if err != nil {
			log.Println(err)
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		if UUID == uuid.Nil {
			w.WriteHeader(http.StatusForbidden)
			return
		}

		request := &pb.UUID{
			Uuid: params["uuid"],
		}

		response, err := g.GetBooksByAuthor(ctx, request)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			log.Println("gRPC error:", err)
			return
		}

		var books []models.Book

		err = json.Unmarshal(response.Books, &books)
		if err != nil {
			log.Println("books unmarshal error:", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		json.NewEncoder(w).Encode(books)
		w.WriteHeader(http.StatusOK)
	}
}

// GetBooksByCategory godoc
// @Tags Filters
// @Summary Returns list of books by category
// @Description Shows all books in the category with provided UUID
// @ID get-books-by-category
// @Produce	json
// @Param uuid path string true "Category UUID"
// @Success 200 {object} []models.Book
// @Failure 400,404 {object} string
// @Failure 500,503 {object} string
// @Failure default {object} string
// @Router /books/category/{uuid} [get]
func (o Orders) GetBooksByCategory(ctx context.Context, g pb.CatalogClient) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		params := mux.Vars(r)
		UUID, err := uuid.FromString(params["uuid"])
		if err != nil {
			log.Println(err)
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		if UUID == uuid.Nil {
			w.WriteHeader(http.StatusForbidden)
			return
		}

		request := &pb.UUID{
			Uuid: params["uuid"],
		}

		response, err := g.GetBooksByCategory(ctx, request)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			log.Println("gRPC error:", err)
			return
		}

		var books []models.Book

		err = json.Unmarshal(response.Books, &books)
		if err != nil {
			log.Println("books unmarshal error:", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		json.NewEncoder(w).Encode(books)
		w.WriteHeader(http.StatusOK)
	}
}
