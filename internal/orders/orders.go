package orders

import "golang-task6/internal/models"

type Orders struct {
	author   models.Author
	book     models.Book
	category models.Category
}
