package grpcserver

import (
	"context"
	"database/sql"
	"encoding/json"
	"golang-task6/internal/models"
	pb "golang-task6/internal/proto"
	"golang-task6/internal/store"
	"log"

	"github.com/gofrs/uuid"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type Catalog struct {
	db *sql.DB
}

func NewCatalog(db *sql.DB) *Catalog {
	s := &Catalog{
		db: db,
	}
	return s
}

func (c *Catalog) GetAuthor(ctx context.Context, id *pb.UUID) (*pb.Author, error) {
	author := models.Author{}

	UUID, err := uuid.FromString(id.GetUuid())
	if err != nil {
		log.Println(err)
		return nil, status.Errorf(codes.InvalidArgument, "incorrect UUID: %v", err)
	}
	author.UUID = UUID
	catalog := store.Catalog{}
	err = catalog.GetAuthor(c.db, &author)
	if err != nil {
		if err == sql.ErrNoRows {
			log.Println(err)
			return nil, status.Errorf(codes.NotFound, "author not found: %v", UUID)
		}
		log.Println(err)
		return nil, status.Error(codes.Internal, "DB error")
	}

	data, err := json.Marshal(author)
	if err != nil {
		return nil, status.Error(codes.Internal, "json encode error")
	}
	return &pb.Author{Author: data}, nil
}

func (c *Catalog) GetAuthors(ctx context.Context, id *pb.Empty) (*pb.Authors, error) {
	catalog := store.Catalog{}
	authors, err := catalog.GetAuthors(c.db)
	if err != nil {
		log.Println(err)
		return nil, status.Error(codes.Internal, "DB error")
	}
	data, err := json.Marshal(authors)
	if err != nil {
		return nil, status.Error(codes.Internal, "json encode error")
	}
	return &pb.Authors{Authors: data}, nil
}

func (c *Catalog) GetCategory(ctx context.Context, id *pb.UUID) (*pb.Category, error) {
	category := models.Category{}

	UUID, err := uuid.FromString(id.GetUuid())
	if err != nil {
		log.Println(err)
		return nil, status.Errorf(codes.InvalidArgument, "incorrect UUID: %v", err)
	}
	category.UUID = UUID
	catalog := store.Catalog{}
	err = catalog.GetCategory(c.db, &category)
	if err != nil {
		if err == sql.ErrNoRows {
			log.Println(err)
			return nil, status.Errorf(codes.NotFound, "category not found: %v", UUID)
		}
		log.Println(err)
		return nil, status.Error(codes.Internal, "DB error")
	}

	data, err := json.Marshal(category)
	if err != nil {
		return nil, status.Error(codes.Internal, "json encode error")
	}
	return &pb.Category{Category: data}, nil
}

func (c *Catalog) GetCategories(ctx context.Context, empty *pb.Empty) (*pb.Categories, error) {
	catalog := store.Catalog{}
	categories, err := catalog.GetCategories(c.db)
	if err != nil {
		log.Println(err)
		return nil, status.Error(codes.Internal, "DB error")
	}
	data, err := json.Marshal(categories)
	if err != nil {
		return nil, status.Error(codes.Internal, "json encode error")
	}
	return &pb.Categories{Categories: data}, nil
}

func (c *Catalog) GetBook(ctx context.Context, id *pb.UUID) (*pb.Book, error) {
	book := models.Book{}

	UUID, err := uuid.FromString(id.GetUuid())
	if err != nil {
		log.Println(err)
		return nil, status.Errorf(codes.InvalidArgument, "incorrect UUID: %v", err)
	}
	book.UUID = UUID
	catalog := store.Catalog{}
	err = catalog.GetBook(c.db, &book)
	if err != nil {
		if err == sql.ErrNoRows {
			log.Println(err)
			return nil, status.Errorf(codes.NotFound, "book not found: %v", UUID)
		}
		log.Println(err)
		return nil, status.Error(codes.Internal, "DB error")
	}

	categories, err := catalog.GetCategoriesOfBook(c.db, &book)
	if err != nil && err != sql.ErrNoRows {
		log.Println(err)
		return nil, status.Error(codes.Internal, "DB error")
	} else {
		err = nil
	}
	book.Categories = categories

	data, err := json.Marshal(book)
	if err != nil {
		return nil, status.Error(codes.Internal, "json encode error")
	}
	return &pb.Book{Book: data}, nil
}

func (c *Catalog) GetBooks(ctx context.Context, empty *pb.Empty) (*pb.Books, error) {
	catalog := store.Catalog{}
	books, err := catalog.GetBooks(c.db)
	if err != nil {
		log.Println(err)
		return nil, status.Error(codes.Internal, "DB error")
	}

	for i, book := range books {
		categories, err := catalog.GetCategoriesOfBook(c.db, &book)
		if err != nil && err != sql.ErrNoRows {
			log.Println(err)
			return nil, status.Error(codes.Internal, "DB error")
		} else {
			err = nil
		}
		books[i].Categories = categories
	}

	data, err := json.Marshal(books)
	if err != nil {
		return nil, status.Error(codes.Internal, "json encode error")
	}
	return &pb.Books{Books: data}, nil
}

func (c *Catalog) GetBooksByAuthor(ctx context.Context, id *pb.UUID) (*pb.Books, error) {
	author := models.Author{}

	UUID, err := uuid.FromString(id.GetUuid())
	if err != nil {
		log.Println(err)
		return nil, status.Errorf(codes.InvalidArgument, "incorrect UUID: %v", err)
	}
	author.UUID = UUID

	catalog := store.Catalog{}
	books, err := catalog.GetBooksByAuthor(c.db, &author)
	if err != nil && err != sql.ErrNoRows {
		log.Println(err)
		return nil, status.Error(codes.Internal, "DB error")
	} else if err != nil {
		log.Println(err)
		return nil, status.Error(codes.NotFound, "books not found")
	}

	for i, book := range books {
		categories, err := catalog.GetCategoriesOfBook(c.db, &book)
		if err != nil && err != sql.ErrNoRows {
			log.Println(err)
			return nil, status.Error(codes.Internal, "DB error")
		} else {
			err = nil
		}
		books[i].Categories = categories
	}

	data, err := json.Marshal(books)
	if err != nil {
		return nil, status.Error(codes.Internal, "json encode error")
	}
	return &pb.Books{Books: data}, nil
}

func (c *Catalog) GetBooksByCategory(ctx context.Context, id *pb.UUID) (*pb.Books, error) {
	category := models.Category{}

	UUID, err := uuid.FromString(id.GetUuid())
	if err != nil {
		log.Println(err)
		return nil, status.Errorf(codes.InvalidArgument, "incorrect UUID: %v", err)
	}
	category.UUID = UUID

	catalog := store.Catalog{}
	books, err := catalog.GetBooksByCategory(c.db, &category)
	if err != nil && err != sql.ErrNoRows {
		log.Println(err)
		return nil, status.Error(codes.Internal, "DB error")
	} else if err != nil {
		log.Println(err)
		return nil, status.Error(codes.NotFound, "books not found")
	}

	for i, book := range books {
		categories, err := catalog.GetCategoriesOfBook(c.db, &book)
		if err != nil && err != sql.ErrNoRows {
			log.Println(err)
			return nil, status.Error(codes.Internal, "DB error")
		} else {
			err = nil
		}
		books[i].Categories = categories
	}

	data, err := json.Marshal(books)
	if err != nil {
		return nil, status.Error(codes.Internal, "json encode error")
	}
	return &pb.Books{Books: data}, nil
}
