package models

type Error struct {
	Code        int    `json:"code"`
	ErrorString string `json:"error"`
}
