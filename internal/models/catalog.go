package models

import "github.com/gofrs/uuid"

type Category struct {
	// in: body
	// id category
	UUID       uuid.UUID `json:"uuid"`
	ParentUUID uuid.UUID `json:"parent_uuid"`
	Name       string    `json:"name"`
}

type Author struct {
	// in: body
	// id author
	UUID uuid.UUID `json:"uuid"`
	Name string    `json:"name"`
}

type Book struct {
	// in: body
	// id book
	UUID       uuid.UUID  `json:"uuid"`
	AuthorUUID uuid.UUID  `json:"author_uuid,omitempty"`
	Title      string     `json:"title"`
	Categories []Category `json:"categories,omitempty"`
}
