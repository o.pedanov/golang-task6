package models

import "github.com/gofrs/uuid"

type Order struct {
	// in: body
	// id author
	UUID        uuid.UUID `json:"uuid"`
	BookUUID    uuid.UUID `json:"book_uuid"` // remove after getting books through gRPC
	Book        Book      `json:"book"`
	Description string    `json:"description"`
}
