package store

import (
	"database/sql"
	"fmt"
	"golang-task6/internal/models"
	"log"
	"time"

	"github.com/gofrs/uuid"
)

func (c *Catalog) GetCategory(db *sql.DB, category *models.Category) error {
	var puuid interface{}
	err := db.QueryRow("SELECT uuid, parent_uuid, name FROM categories WHERE uuid=$1 AND deleted_at IS NULL;", category.UUID).Scan(&category.UUID, &puuid, &category.Name)
	if err != nil {
		log.Println("Category not found", err)
		return err
	}

	// If ParentUUID is NULL in DB
	scanErr := category.ParentUUID.Scan(puuid)
	if scanErr != nil {
		category.ParentUUID = uuid.Nil
	} else {
		var categoryUUID uuid.UUID
		err := categoryUUID.Scan(puuid)
		if err != nil {
			log.Println(err)
			return err
		}
	}
	return nil
}

func (c *Catalog) GetCategories(db *sql.DB) ([]models.Category, error) {
	var categories []models.Category
	rows, err := db.Query("SELECT uuid, parent_uuid, name FROM categories WHERE deleted_at IS NULL;")
	if err != nil {
		return categories, err
	}
	defer rows.Close()

	for rows.Next() {
		var puuid interface{}
		category := &models.Category{}
		rows.Scan(&category.UUID, &puuid, &category.Name)
		scanErr := category.ParentUUID.Scan(puuid)
		if scanErr != nil {
			category.ParentUUID = uuid.UUID{}
		}
		categories = append(categories, *category)
	}
	return categories, nil
}

func (c *Catalog) AddCategory(db *sql.DB, category *models.Category) error {
	var err error = nil
	newUUID, err := uuid.NewV4()
	if err != nil {
		fmt.Printf("Failed to generate UUID: %v\n", err)
		return err
	}
	category.UUID = newUUID

	_, err = db.Exec("INSERT INTO categories (uuid, parent_uuid, name) VALUES ($1, $2, $3);", category.UUID, category.ParentUUID, category.Name)
	if err != nil {
		return err
	}
	return nil
}

func (c *Catalog) UpdateCategory(db *sql.DB, category *models.Category) error {

	_, err := db.Exec("UPDATE categories SET name=$2, parent_uuid=$3, updated_at=$4 WHERE uuid=$1;", category.UUID, category.Name, category.ParentUUID, time.Now())
	if err != nil {
		return err
	}

	return nil
}

func (c *Catalog) DeleteCategory(db *sql.DB, category *models.Category) error {
	// Update child categories
	rows, err := db.Query("SELECT uuid, name FROM categories WHERE parent_uuid=$1", category.UUID)
	if err != sql.ErrNoRows {
		for rows.Next() {
			cat := models.Category{}
			rows.Scan(&cat.UUID, &cat.Name)
			cat.ParentUUID = uuid.Nil
			err = c.UpdateCategory(db, &cat)
			if err != nil {
				return err
			}
		}
	} else if err != nil {
		return err
	}
	rows.Close()

	// Update category deleted_at in DB
	_, err = db.Exec("UPDATE categories SET deleted_at=$2 WHERE uuid=$1;", category.UUID, time.Now())
	if err != nil {
		return err
	}

	return nil
}

func (c *Catalog) GetCategoriesOfBook(db *sql.DB, book *models.Book) ([]models.Category, error) {
	var categories []models.Category
	rows, err := db.Query(`
	SELECT uuid, name, parent_uuid
	FROM categories
	JOIN books_categories ON uuid = books_categories.category_uuid
	WHERE books_categories.book_uuid = $1	
	;`, book.UUID)
	if err != nil {
		return categories, err
	}
	defer rows.Close()

	for rows.Next() {
		category := models.Category{}
		err = rows.Scan(&category.UUID, &category.Name, &category.ParentUUID)
		if err != nil {
			return categories, err
		}
		categories = append(categories, category)
	}
	return categories, nil
}
