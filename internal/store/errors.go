package store

import "errors"

var (
	ErrParentCategoryNotFound = errors.New("parent category not found")
)
