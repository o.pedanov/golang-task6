package store

import (
	"database/sql"
	"errors"
	"fmt"
	"golang-task6/internal/models"
	"log"
	"time"

	"github.com/gofrs/uuid"
)

func (c *Catalog) AddBook(db *sql.DB, book *models.Book) error {
	newUUID, err := uuid.NewV4()
	if err != nil {
		fmt.Printf("Failed to generate UUID: %v\n", err)
		return err
	}

	book.UUID = newUUID
	_, err = db.Exec("INSERT INTO books (uuid, author_uuid, title) VALUES ($1, $2, $3);", book.UUID, book.AuthorUUID, book.Title)
	if err != nil {
		return err
	}

	// Connect book to categories
	for _, categories := range book.Categories {
		categoryUUID := categories.UUID
		_, err = db.Exec("INSERT INTO books_categories (book_uuid, category_uuid) VALUES ($1, $2)", book.UUID, categoryUUID)
		if err != nil {
			return err
		}
	}
	return nil
}

func (c *Catalog) AddBookToCategory(db *sql.DB, book *models.Book, category *models.Category) error {
	// Book already connected to category?
	err := db.QueryRow("SELECT book_uuid, category_uuid FROM books_categories WHERE book_uuid=$1 AND category_uuid=$2;", book.UUID, category.UUID).Scan(&book.UUID, &category.UUID)
	if err != nil {
		if err != sql.ErrNoRows {
			log.Println(err)
			return err
		}
	} else {
		err = errors.New("book already in the category")
		log.Println(err)
		return err
	}
	_, err = db.Exec(`INSERT INTO books_categories (book_uuid, category_uuid) VALUES ($1, $2);`, book.UUID, category.UUID)
	if err != nil {
		return err
	}
	return nil
}

func (c *Catalog) RemoveBookFromCategory(db *sql.DB, book *models.Book, category *models.Category) error {
	// Book is connected to category?
	err := db.QueryRow("SELECT book_uuid, category_uuid FROM books_categories WHERE book_uuid=$1 AND category_uuid=$2;", book.UUID, category.UUID).Scan(&book.UUID, &category.UUID)
	if err != nil {
		return err
	}

	// Remove book from category
	_, err = db.Exec("DELETE FROM books_categories WHERE book_uuid=$1 AND category_uuid=$2;", book.UUID, category.UUID)
	if err != nil {
		return err
	}

	return nil
}

func (c *Catalog) UpdateBook(db *sql.DB, book *models.Book) error {
	_, err := db.Exec("UPDATE books SET title=$2, author_uuid=$3 WHERE uuid=$1;", book.UUID, book.Title, book.AuthorUUID)
	if err != nil {
		return err
	}

	return nil
}

func (c *Catalog) DeleteBook(db *sql.DB, book *models.Book) error {
	_, err := db.Exec("UPDATE books SET deleted_at=$2 WHERE uuid=$1;", book.UUID, time.Now())
	if err != nil {
		return err
	}

	return nil
}

func (c *Catalog) GetBook(db *sql.DB, book *models.Book) error {
	// Get book from DB
	err := db.QueryRow("SELECT uuid, author_uuid, title FROM books WHERE uuid=$1 AND deleted_at IS NULL;", book.UUID).Scan(&book.UUID, &book.AuthorUUID, &book.Title)
	if err == sql.ErrNoRows {
		// Book not found
		return err
	} else if err != nil {
		return err
	}
	return nil
}

func (c *Catalog) GetBooks(db *sql.DB) ([]models.Book, error) {
	var books []models.Book
	rows, err := db.Query("SELECT uuid, title, author_uuid FROM books WHERE deleted_at IS NULL;")
	if err != nil {
		return books, err
	}
	defer rows.Close()

	for rows.Next() {
		book := models.Book{}
		err = rows.Scan(&book.UUID, &book.Title, &book.AuthorUUID)
		if err != nil {
			return books, err
		}
		books = append(books, book)
	}
	return books, nil
}

func (c *Catalog) GetBooksByAuthor(db *sql.DB, author *models.Author) ([]models.Book, error) {
	var books []models.Book

	rows, err := db.Query("SELECT uuid, title, author_uuid FROM books WHERE author_uuid = $1 AND deleted_at IS NULL;", author.UUID)
	if err != nil {
		return books, err
	}
	defer rows.Close()

	for rows.Next() {
		var book = models.Book{}
		err = rows.Scan(&book.UUID, &book.Title, &book.AuthorUUID)
		if err != nil {
			return books, err
		}
		books = append(books, book)
	}
	return books, nil
}

func (c *Catalog) GetBooksByCategory(db *sql.DB, category *models.Category) ([]models.Book, error) {
	var books []models.Book
	rows, err := db.Query(`
	SELECT uuid, title, author_uuid
	FROM books
	JOIN books_categories ON uuid = books_categories.book_uuid
	WHERE books_categories.category_uuid = $1	
	;`, category.UUID)
	if err != nil {
		return books, err
	}
	defer rows.Close()

	for rows.Next() {
		book := models.Book{}
		err = rows.Scan(&book.UUID, &book.Title, &book.AuthorUUID)
		if err != nil {
			return books, err
		}
		books = append(books, book)
	}
	return books, nil
}

func (c *Catalog) DeleteCategoryConnections(db *sql.DB, book *models.Book) error {
	_, err := db.Exec("DELETE FROM books_categories WHERE book_uuid = $1;", book.UUID)
	if err != nil {
		return err
	}

	return nil
}
