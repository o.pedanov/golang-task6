package store

import (
	"database/sql"
	"golang-task6/internal/models"
	"log"
	"time"

	"github.com/gofrs/uuid"
)

func (o *Orders) GetOrder(db *sql.DB, order *models.Order) error {
	// Get author from store DB
	err := db.QueryRow("SELECT book_uuid, description FROM orders WHERE uuid=$1 AND deleted_at IS NULL;", order.UUID).Scan(&order.BookUUID, &order.Description)
	if err != nil {
		return err
	}
	return nil
}

func (o *Orders) GetOrders(db *sql.DB) ([]models.Order, error) {
	var orders []models.Order
	rows, err := db.Query("SELECT uuid, book_uuid, description FROM orders WHERE deleted_at IS NULL;")
	if err != nil {
		return orders, err
	}
	defer rows.Close()

	for rows.Next() {
		var order models.Order
		if err := rows.Scan(&order.UUID, &order.BookUUID, &order.Description); err != nil {
			return orders, err
		}
		orders = append(orders, order)
	}
	return orders, nil
}

func (o *Orders) AddOrder(db *sql.DB, order *models.Order) error {
	// Generate UUID
	newUUID, err := uuid.NewV4()
	if err != nil {
		log.Printf("Failed to generate UUID: %v\n", err)
		return err
	}
	order.UUID = newUUID

	// Add author to store.db
	_, err = db.Exec("INSERT INTO orders (uuid, book_uuid, description) VALUES ($1, $2, $3);", order.UUID, order.BookUUID, order.Description)
	if err != nil {
		return err
	}
	return nil
}

func (o *Orders) UpdateOrder(db *sql.DB, order *models.Order) error {
	_, err := db.Exec("UPDATE orders SET book_uuid=$2, description=$3, updated_at=$4 WHERE uuid=$1;", order.UUID, order.BookUUID, order.Description, time.Now())
	if err != nil {
		return err
	}

	return nil
}

func (o *Orders) DeleteOrder(db *sql.DB, order *models.Order) error {
	_, err := db.Exec("UPDATE orders SET deleted_at=$2 WHERE uuid=$1;", order.UUID, time.Now())
	if err != nil {
		return err
	}

	return nil
}
