package store

import (
	"database/sql"
	"golang-task6/internal/models"
	"log"
	"time"

	"github.com/gofrs/uuid"
)

func (c *Catalog) GetAuthor(db *sql.DB, author *models.Author) error {
	// Get author from store DB
	err := db.QueryRow("SELECT uuid, name FROM authors WHERE uuid=$1 AND deleted_at IS NULL;", author.UUID).Scan(&author.UUID, &author.Name)
	if err != nil {
		return err
	}

	return nil
}

func (c *Catalog) GetAuthors(db *sql.DB) ([]models.Author, error) {
	var authors []models.Author
	rows, err := db.Query("SELECT uuid, name FROM authors WHERE deleted_at IS NULL;")
	if err != nil {
		return authors, err
	}
	defer rows.Close()

	for rows.Next() {
		var author models.Author
		if err := rows.Scan(&author.UUID, &author.Name); err != nil {
			return authors, nil
		}
		authors = append(authors, author)
	}
	return authors, nil
}

func (c *Catalog) AddAuthor(db *sql.DB, author *models.Author) error {
	// Generate UUID
	newUUID, err := uuid.NewV4()
	if err != nil {
		log.Printf("Failed to generate UUID: %v\n", err)
		return err
	}
	author.UUID = newUUID

	// Add author to store.db
	_, err = db.Exec("INSERT INTO authors (uuid, name) VALUES ($1, $2);", author.UUID, author.Name)
	if err != nil {
		return err
	}
	return nil
}

func (c *Catalog) UpdateAuthor(db *sql.DB, author *models.Author) error {
	_, err := db.Exec("UPDATE authors SET name=$2, updated_at=$3 WHERE uuid=$1;", author.UUID, author.Name, time.Now())
	if err != nil {
		return err
	}

	return nil
}

func (c *Catalog) DeleteAuthor(db *sql.DB, author *models.Author) error {
	// Update author deleted_at in store DB
	_, err := db.Exec("UPDATE authors SET deleted_at=$2 WHERE uuid=$1;", author.UUID, time.Now())
	if err != nil {
		return err
	}

	return nil
}
