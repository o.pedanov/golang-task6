package migrations

import (
	"database/sql"

	"github.com/pressly/goose"
)

func init() {
	goose.AddMigration(upOrders, downOrders)
}

func upOrders(tx *sql.Tx) error {
	// This code is executed when the migration is applied.
	_, err := tx.Exec(`
	DROP TABLE IF EXISTS orders;
	CREATE TABLE orders (
		id SERIAL NOT NULL,
		created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
		updated_at TIMESTAMP,
		deleted_at TIMESTAMP,
		uuid VARCHAR(36) NOT NULL,
		book_uuid VARCHAR(36) NOT NULL,
		description VARCHAR(100),
		PRIMARY KEY (uuid)
		);`)
	if err != nil {
		return err
	}
	return nil
}

func downOrders(tx *sql.Tx) error {
	// This code is executed when the migration is rolled back.
	_, err := tx.Exec(`DROP TABLE IF EXISTS orders;`)
	if err != nil {
		return err
	}
	return nil
}
