package migrations

import (
	"database/sql"

	"github.com/pressly/goose"
)

func init() {
	goose.AddMigration(upBooks, downBooks)
}

func upBooks(tx *sql.Tx) error {
	// This code is executed when the migration is applied.
	_, err := tx.Exec(`
	DROP TABLE IF EXISTS books;
	CREATE TABLE books (
		id SERIAL NOT NULL,
		created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
		updated_at TIMESTAMP,
		deleted_at TIMESTAMP,
		title VARCHAR(100),
		uuid VARCHAR(36) NOT NULL,
		author_uuid VARCHAR(36) NOT NULL,
		PRIMARY KEY (uuid),
		FOREIGN KEY (author_uuid) REFERENCES authors(uuid) ON DELETE SET NULL
		);`)
	if err != nil {
		return err
	}

	_, err = tx.Exec(`
	DROP TABLE IF EXISTS books_categories;
	CREATE TABLE books_categories (
		book_uuid VARCHAR(36) NOT NULL,
		category_uuid VARCHAR(36) NOT NULL,
		PRIMARY KEY (book_uuid, category_uuid),
		FOREIGN KEY (book_uuid) REFERENCES books(uuid) ON UPDATE CASCADE,
		FOREIGN KEY (category_uuid) REFERENCES categories(uuid) ON UPDATE CASCADE
		);`)
	if err != nil {
		return err
	}
	return nil
}

func downBooks(tx *sql.Tx) error {
	// This code is executed when the migration is rolled back.
	_, err := tx.Exec(`
	DROP TABLE books_categories;
	DROP TABLE books;	
	`)
	if err != nil {
		return err
	}
	return nil
}
