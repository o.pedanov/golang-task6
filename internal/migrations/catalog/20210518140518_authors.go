package migrations

import (
	"database/sql"

	"github.com/pressly/goose"
)

func init() {
	goose.AddMigration(upAuthors, downAuthors)
}

func upAuthors(tx *sql.Tx) error {
	// This code is executed when the migration is applied.
	_, err := tx.Exec(`
	DROP TABLE IF EXISTS authors;
	CREATE TABLE authors (
		id SERIAL NOT NULL,
		created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
		updated_at TIMESTAMP,
		deleted_at TIMESTAMP,
		name VARCHAR(100),
		uuid VARCHAR(36) NOT NULL,
		PRIMARY KEY (uuid)
		);`)
	if err != nil {
		return err
	}
	return nil
}

func downAuthors(tx *sql.Tx) error {
	// This code is executed when the migration is rolled back.
	_, err := tx.Exec(`DROP TABLE authors;`)
	if err != nil {
		return err
	}
	return nil
}
