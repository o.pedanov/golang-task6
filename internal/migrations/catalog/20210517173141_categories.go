package migrations

import (
	"database/sql"

	"github.com/pressly/goose"
)

func init() {
	goose.AddMigration(upCategories, downCategories)
}

func upCategories(tx *sql.Tx) error {
	// This code is executed when the migration is applied.
	_, err := tx.Exec(`
	DROP TABLE IF EXISTS categories;
	CREATE TABLE categories (
		id SERIAL NOT NULL,
		created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
		updated_at TIMESTAMP,
		deleted_at TIMESTAMP,
		name VARCHAR(100),
		uuid VARCHAR(36) NOT NULL,
		parent_uuid VARCHAR(36),
		PRIMARY KEY (uuid),
		FOREIGN KEY (parent_uuid) REFERENCES categories(uuid)
		);
	INSERT INTO categories (uuid, name) VALUES ('00000000-0000-0000-0000-000000000000', '');`)
	if err != nil {
		return err
	}
	return nil
}

func downCategories(tx *sql.Tx) error {
	// This code is executed when the migration is rolled back.
	_, err := tx.Exec(`DROP TABLE categories;`)
	if err != nil {
		return err
	}
	return nil
}
