package catalog

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"golang-task6/internal/models"
	"golang-task6/internal/store"
	"log"
	"net/http"

	"github.com/gofrs/uuid"
	"github.com/gorilla/mux"
)

// GetAuthors godoc
// @Tags Authors
// @Summary Returns list of authors
// @Description Shows all authors
// @ID get-authors
// @Produce	json
// @Success 200 {object} []models.Author
// @Failure 400,404 {object} string
// @Failure 500,503 {object} string
// @Failure default {object} string
// @Router /authors [get]
func (c Catalog) GetAuthors(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		catalog := store.Catalog{}
		authors, err := catalog.GetAuthors(db)
		if err != nil {
			fmt.Println(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(authors)
	}
}

// GetAuthor godoc
// @Tags Authors
// @Summary Returns author
// @Description Shows an author based on UUID
// @ID get-author
// @Produce	json
// @Param uuid path string true "Author UUID"
// @Success 200 {object} models.Author
// @Failure 400,404 {object} string
// @Failure 500,503 {object} string
// @Failure default {object} string
// @Router /authors/{uuid} [get]
func (c Catalog) GetAuthor(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		author := models.Author{}

		params := mux.Vars(r)
		UUID, err := uuid.FromString(params["uuid"])
		if err != nil {
			log.Println(err)
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		if UUID == uuid.Nil {
			w.WriteHeader(http.StatusForbidden)
			return
		}

		author.UUID = UUID
		catalog := store.Catalog{}
		err = catalog.GetAuthor(db, &author)
		if err != nil {
			if err == sql.ErrNoRows {
				log.Println(err)
				http.Error(w, err.Error(), http.StatusNotFound)
				return
			}
			log.Println(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(author)

	}
}

// AddAuthor godoc
// @Tags Authors
// @Summary Creates Author
// @Description Create new author based on given name
// @ID create-author
// @Accept	json
// @Produce	json
// @Param author body models.Author true "New author name"
// @Success 200 {object} models.Author
// @Failure 400,404 {object} string
// @Failure 500,503 {object} string
// @Failure default {object} string
// @Router /authors [post]
func (c Catalog) AddAuthor(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		author := models.Author{}

		err := json.NewDecoder(r.Body).Decode(&author)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			log.Println("JSON Decode error:", err)
			return
		}
		if len(author.Name) == 0 {
			log.Println("Author name is empty")
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		catalog := store.Catalog{}
		err = catalog.AddAuthor(db, &author)
		if err != nil {
			log.Println(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(author)
	}
}

// UpdateAuthor godoc
// @Tags Authors
// @Summary Updates author
// @Description Updates an author based on UUID
// @ID update-author
// @Accept	json
// @Produce	json
// @Param uuid path string true "Author UUID"
// @Param author body models.Author true "Author data"
// @Success 200 {object} models.Author
// @Failure 400,404 {object} string
// @Failure 500,503 {object} string
// @Failure default {object} string
// @Router /authors/{uuid} [put]
func (c Catalog) UpdateAuthor(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		author := models.Author{}

		params := mux.Vars(r)
		UUID, err := uuid.FromString(params["uuid"])
		if err != nil {
			log.Println(err)
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		err = json.NewDecoder(r.Body).Decode(&author)
		if err != nil {
			log.Println("JSON Decode error:", err)
			http.Error(w, err.Error(), http.StatusBadRequest)
		}

		author.UUID = UUID
		catalog := store.Catalog{}
		err = catalog.UpdateAuthor(db, &author)
		if err != nil {
			log.Println(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(author)
	}
}

// DeleteAuthor godoc
// @Tags Authors
// @Summary Deletes author
// @Description Deletes an author based on UUID
// @ID delete-author
// @Produce	json
// @Param uuid path string true "Author UUID"
// @Success 200
// @Failure 400,404 {object} string
// @Failure 500,503 {object} string
// @Failure default {object} string
// @Router /authors/{uuid} [delete]
func (c Catalog) DeleteAuthor(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		author := models.Author{}
		params := mux.Vars(r)

		UUID, err := uuid.FromString(params["uuid"])
		if err != nil {
			log.Println(err)
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		author.UUID = UUID
		catalog := store.Catalog{}
		err = catalog.GetAuthor(db, &author)
		if err != nil {
			if err != nil {
				http.Error(w, err.Error(), http.StatusNotFound)
				return
			}
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		err = catalog.DeleteAuthor(db, &author)
		if err != nil {
			log.Println(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.WriteHeader(http.StatusOK)
	}
}
