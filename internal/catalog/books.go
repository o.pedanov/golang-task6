package catalog

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"golang-task6/internal/models"
	"golang-task6/internal/store"
	"log"
	"net/http"

	"github.com/gofrs/uuid"
	"github.com/gorilla/mux"
)

// GetBooks godoc
// @Tags Books
// @Summary Returns list of books
// @Description Shows all books
// @ID get-books
// @Produce	json
// @Success 200 {object} []models.Book
// @Failure 400,404 {object} string
// @Failure 500,503 {object} string
// @Failure default {object} string
// @Router /books [get]
func (c Catalog) GetBooks(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		catalog := store.Catalog{}

		// Get slice of bare books
		books, err := catalog.GetBooks(db)
		if err != nil {
			fmt.Println(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		// Append Categories data
		for i, book := range books {
			categories, err := catalog.GetCategoriesOfBook(db, &book)
			if err != nil {
				log.Println(err)
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			var bookCategories []models.Category
			for _, category := range categories {
				err := catalog.GetCategory(db, &category)
				if err != nil && err != sql.ErrNoRows {
					fmt.Println(err)
					http.Error(w, err.Error(), http.StatusInternalServerError)
					return
				}
				if err != sql.ErrNoRows {
					bookCategories = append(bookCategories, category)
				}

			}
			books[i].Categories = bookCategories
		}
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(books)
	}
}

// GetBook godoc
// @Tags Books
// @Summary Returns book
// @Description Shows a book based on UUID
// @ID get-book
// @Produce	json
// @Param uuid path string true "Book UUID"
// @Success 200 {object} models.Book
// @Failure 400,404 {object} string
// @Failure 500,503 {object} string
// @Failure default {object} string
// @Router /books/{uuid} [get]
func (c Catalog) GetBook(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		book := models.Book{}

		params := mux.Vars(r)
		UUID, err := uuid.FromString(params["uuid"])
		if err != nil {
			log.Println(err)
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		book.UUID = UUID
		catalog := store.Catalog{}
		err = catalog.GetBook(db, &book)
		if err != nil {
			if err != sql.ErrNoRows {
				log.Println(err)
				http.Error(w, err.Error(), http.StatusNotFound)
				return
			}
			log.Println(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		categories, err := catalog.GetCategoriesOfBook(db, &book)
		if err != nil {
			log.Println(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		var bookCategories []models.Category
		for _, category := range categories {
			err = catalog.GetCategory(db, &category)
			if err != nil && err != sql.ErrNoRows {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			if err != sql.ErrNoRows {
				bookCategories = append(bookCategories, category)
			}
		}

		book.Categories = bookCategories
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(book)

	}
}

// AddBook godoc
// @Tags Books
// @Summary Creates a book
// @Description Create new book based on given title, author and categories
// @ID create-book
// @Accept	json
// @Produce	json
// @Param book body models.Book true "New book title, [author UUID] and [categories UUIDs]"
// @Success 200 {object} models.Book
// @Failure 400,404 {object} string
// @Failure 500,503 {object} string
// @Failure default {object} string
// @Router /books [post]
func (c Catalog) AddBook(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var book models.Book

		err := json.NewDecoder(r.Body).Decode(&book)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			log.Println("JSON Decode error:", err)
			return
		}

		if len(book.Title) == 0 {
			log.Println("Name is empty")
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		catalog := store.Catalog{}

		// Author exist?
		if book.AuthorUUID != uuid.Nil {
			author := models.Author{}
			author.UUID = book.AuthorUUID
			err = catalog.GetAuthor(db, &author)
			if err != nil {
				if err == sql.ErrNoRows {
					log.Println("Author not found")
					http.Error(w, err.Error(), http.StatusNotFound)
					return
				}
				log.Println(err)
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
		}

		// Categories exists?
		if len(book.Categories) != 0 {
			for _, category := range book.Categories {
				if category.UUID != uuid.Nil {
					err = catalog.GetCategory(db, &category)
					if err != nil {
						log.Println("Parent category not found")
						http.Error(w, err.Error(), http.StatusNotAcceptable)
						return
					}
				}
			}
		}

		err = catalog.AddBook(db, &book)
		if err != nil {
			log.Println(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(book)
	}
}

// UpdateBook godoc
// @Tags Books
// @Summary Updates a book
// @Description Updates a book's title and/or author based on UUID
// @Description OR
// @Description Adds book to specified category IF provided
// @ID update-book
// @Accept	json
// @Produce	json
// @Param uuid path string true "Book UUID"
// @Param category query string false "Category UUID"
// @Param book body models.Book false "Book data"
// @Success 200 {object} models.Book
// @Failure 400,404 {object} string
// @Failure 500,503 {object} string
// @Failure default {object} string
// @Router /books/{uuid} [put]
func (c Catalog) UpdateBook(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var (
			book, updatedBook models.Book
			category          models.Category
		)

		params := mux.Vars(r)
		bookUUID, err := uuid.FromString(params["uuid"])
		if err != nil {
			log.Println(err)
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		keys, ok := r.URL.Query()["category"]

		if !ok || len(keys[0]) < 0 {
			log.Println("No category provided")
		} else {
			categoryUUID, err := uuid.FromString(keys[0])
			if err != nil {
				log.Println(err)
				http.Error(w, err.Error(), http.StatusBadRequest)
				return
			}

			// Forbid to update books category to root
			if categoryUUID == uuid.Nil {
				http.Error(w, err.Error(), http.StatusForbidden)
				return
			}

			catalog := store.Catalog{}

			book.UUID = bookUUID
			// Book exist?
			err = catalog.GetBook(db, &book)
			if err != nil {
				if err == sql.ErrNoRows {
					log.Println("Book not found")
					http.Error(w, err.Error(), http.StatusNotFound)
					return
				}
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}

			category.UUID = categoryUUID
			// Category exist?
			err = catalog.GetCategory(db, &category)
			if err != nil {
				if err == sql.ErrNoRows {
					log.Println("Category not found")
					http.Error(w, err.Error(), http.StatusNotFound)
					return
				}
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}

			err = catalog.AddBookToCategory(db, &book, &category)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			w.WriteHeader(http.StatusOK)
			json.NewEncoder(w).Encode(book)
			return
		}

		err = json.NewDecoder(r.Body).Decode(&updatedBook)
		if err != nil {
			log.Println("JSON Decode error:", err)
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		book.UUID = bookUUID
		catalog := store.Catalog{}
		err = catalog.GetBook(db, &book)
		if err != nil {
			if err == sql.ErrNoRows {
				log.Println(err)
				http.Error(w, err.Error(), http.StatusNotFound)
				return
			}
			log.Println(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		updatedBook.UUID = bookUUID
		err = catalog.UpdateBook(db, &updatedBook)
		if err != nil {
			log.Println(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(updatedBook)
	}
}

// DeleteBook godoc
// @Tags Books
// @Summary Deletes book
// @Description Deletes a book based on UUID
// @ID delete-book
// @Produce	json
// @Param uuid path string true "Book UUID"
// @Success 200
// @Failure 400,404 {object} string
// @Failure 500,503 {object} string
// @Failure default {object} string
// @Router /books/{uuid} [delete]
func (c Catalog) DeleteBook(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		book := models.Book{}

		params := mux.Vars(r)
		UUID, err := uuid.FromString(params["uuid"])
		if err != nil {
			log.Println(err)
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		if UUID == uuid.Nil {
			w.WriteHeader(http.StatusForbidden)
			return
		}

		book.UUID = UUID
		catalog := store.Catalog{}

		// Book exist?
		err = catalog.GetBook(db, &book)
		if err != nil {
			if err == sql.ErrNoRows {
				log.Println(err)
				http.Error(w, err.Error(), http.StatusNotFound)
			}
			log.Println(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		// Delete book category connections
		err = catalog.DeleteCategoryConnections(db, &book)
		if err != nil {
			log.Println(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		err = catalog.DeleteBook(db, &book)
		if err != nil {
			log.Println(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.WriteHeader(http.StatusOK)
	}
}

// GetBooksByCategory godoc
// @Tags Books
// @Summary Returns list of books filtered by category
// @Description Shows a list of books filtered by provided category UUID
// @ID get-books-by-category
// @Produce	json
// @Param uuid path string true "Category UUID"
// @Success 200 {object} []models.Book
// @Failure 400,404 {object} string
// @Failure 500,503 {object} string
// @Failure default {object} string
// @Router /books/category/{uuid} [get]
func (c Catalog) GetBooksByCategory(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		category := models.Category{}

		params := mux.Vars(r)
		UUID, err := uuid.FromString(params["uuid"])
		if err != nil {
			log.Println(err)
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		category.UUID = UUID
		catalog := store.Catalog{}

		// Get slice of bare books
		books, err := catalog.GetBooksByCategory(db, &category)
		if err != nil {
			fmt.Println(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		// Append Categories data
		for i, book := range books {
			categories, err := catalog.GetCategoriesOfBook(db, &book)
			if err != nil {
				log.Println(err)
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			var bookCategories []models.Category
			for _, category := range categories {
				err := catalog.GetCategory(db, &category)
				if err != nil && err != sql.ErrNoRows {
					fmt.Println(err)
					http.Error(w, err.Error(), http.StatusInternalServerError)
					return
				}
				if err != sql.ErrNoRows {
					bookCategories = append(bookCategories, category)
				}

			}
			books[i].Categories = bookCategories
		}
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(books)
	}
}

// GetBooksByAuthor godoc
// @Tags Books
// @Summary Returns list of books filtered by Author
// @Description Shows books filtered by provided Author UUID
// @ID get-books-by-author
// @Produce	json
// @Param uuid path string true "Author UUID"
// @Success 200 {object} []models.Book
// @Failure 400,404 {object} string
// @Failure 500,503 {object} string
// @Failure default {object} string
// @Router /books/author/{uuid} [get]
func (c Catalog) GetBooksByAuthor(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		author := models.Author{}

		params := mux.Vars(r)
		UUID, err := uuid.FromString(params["uuid"])
		if err != nil {
			log.Println(err)
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		author.UUID = UUID
		catalog := store.Catalog{}

		// Get slice of bare books
		books, err := catalog.GetBooksByAuthor(db, &author)
		if err != nil {
			fmt.Println(err)
			http.Error(w, err.Error(), http.StatusServiceUnavailable)
			return
		}

		// Append Categories data
		for i, book := range books {
			categories, err := catalog.GetCategoriesOfBook(db, &book)
			if err != nil {
				log.Println(err)
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			var bookCategories []models.Category
			for _, category := range categories {
				err := catalog.GetCategory(db, &category)
				if err != nil && err != sql.ErrNoRows {
					fmt.Println(err)
					http.Error(w, err.Error(), http.StatusInternalServerError)
					return
				}
				if err != sql.ErrNoRows {
					bookCategories = append(bookCategories, category)
				}

			}
			books[i].Categories = bookCategories
		}
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(books)
	}
}

// RemoveBookFromCategory godoc
// @Tags Books
// @Summary Remove book from category
// @Description Removes book from specified category
// @ID remove-book-from-category
// @Accept	json
// @Produce	json
// @Param uuid path string true "Book UUID"
// @Param category query string false "Category UUID"
// @Success 200 {object} models.Book
// @Failure 400,404 {object} string
// @Failure 500,503 {object} string
// @Failure default {object} string
// @Router /books/{uuid} [patch]
func (c Catalog) RemoveBookFromCategory(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var (
			book     models.Book
			category models.Category
		)

		params := mux.Vars(r)
		bookUUID, err := uuid.FromString(params["uuid"])
		if err != nil {
			log.Println(err)
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		keys, ok := r.URL.Query()["category"]

		if !ok || len(keys[0]) < 0 {
			log.Println("No category provided")
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		} else {
			categoryUUID, err := uuid.FromString(keys[0])
			if err != nil {
				log.Println(err)
				http.Error(w, err.Error(), http.StatusBadRequest)
				return
			}

			catalog := store.Catalog{}
			book.UUID = bookUUID
			category.UUID = categoryUUID

			err = catalog.RemoveBookFromCategory(db, &book, &category)
			if err != nil {
				if err == sql.ErrNoRows {
					log.Println("Book is not in the category")
					http.Error(w, err.Error(), http.StatusNotFound)
					return
				}
				log.Println("Remove book from category error:", err)
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			w.WriteHeader(http.StatusOK)
			json.NewEncoder(w).Encode(book)
			return
		}
	}

}
