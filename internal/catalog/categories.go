package catalog

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"golang-task6/internal/models"
	"golang-task6/internal/store"
	"log"
	"net/http"

	"github.com/gofrs/uuid"
	"github.com/gorilla/mux"
)

// GetCategories godoc
// @Tags Categories
// @Summary Returns list of categories
// @Description Shows all books categories
// @ID get-categories
// @Produce	json
// @Success 200 {object} []models.Category
// @Failure 400,404 {object} string
// @Failure 500,503 {object} string
// @Failure default {object} string
// @Router /categories [get]
func (c Catalog) GetCategories(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		catalog := store.Catalog{}
		data, err := catalog.GetCategories(db)
		if err != nil {
			fmt.Println(err)
			http.Error(w, err.Error(), http.StatusServiceUnavailable)
		}
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(data)
	}
}

// GetCategory godoc
// @Tags Categories
// @Summary Returns category
// @Description Shows a books category based on UUID
// @ID get-category
// @Produce	json
// @Param uuid path string true "Category UUID"
// @Success 200 {object} models.Category
// @Failure 400,404 {object} string
// @Failure 500,503 {object} string
// @Failure default {object} string
// @Router /categories/{uuid} [get]
func (c Catalog) GetCategory(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		category := models.Category{}
		params := mux.Vars(r)
		UUID, err := uuid.FromString(params["uuid"])
		if err != nil {
			log.Println(err)
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		if UUID == uuid.Nil {
			w.WriteHeader(http.StatusForbidden)
			return
		}
		category.UUID = UUID
		catalog := store.Catalog{}
		err = catalog.GetCategory(db, &category)
		if err != nil {
			log.Println(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(category)

	}
}

// AddCategory godoc
// @Tags Categories
// @Summary Creates books Category
// @Description Create new books category based on given name and [parent_uuid]
// @ID create-category
// @Accept	json
// @Produce	json
// @Param category body models.Category true "New category name and [parent_uuid]"
// @Success 200 {object} models.Category
// @Failure 400,404 {object} string
// @Failure 500,503 {object} string
// @Failure default {object} string
// @Router /categories [post]
func (c Catalog) AddCategory(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var category models.Category

		err := json.NewDecoder(r.Body).Decode(&category)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			log.Println("JSON Decode error:", err)
			return
		}
		if len(category.Name) == 0 {
			log.Println("Name is empty")
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		catalog := store.Catalog{}

		// Parent Category exist?
		if category.ParentUUID != uuid.Nil {
			pCategory := models.Category{}
			pCategory.UUID = category.ParentUUID
			err = catalog.GetCategory(db, &pCategory)
			if err != nil {
				log.Println("Parent category not found")
				http.Error(w, err.Error(), http.StatusNotAcceptable)
				return
			}
		}

		err = catalog.AddCategory(db, &category)
		if err != nil {
			log.Println(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(category)
	}
}

// UpdateCategory godoc
// @Tags Categories
// @Summary Updates category
// @Description Updates a books category based on UUID
// @ID update-category
// @Accept	json
// @Produce	json
// @Param uuid path string true "Category UUID"
// @Param category body models.Category true "Category data"
// @Success 200 {object} models.Category
// @Failure 400,404 {object} string
// @Failure 500,503 {object} string
// @Failure default {object} string
// @Router /categories/{uuid} [put]
func (c Catalog) UpdateCategory(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var category models.Category

		params := mux.Vars(r)
		UUID, err := uuid.FromString(params["uuid"])
		if err != nil {
			log.Println(err)
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		err = json.NewDecoder(r.Body).Decode(&category)
		if err != nil {
			log.Println("JSON Decode error:", err)
			http.Error(w, err.Error(), http.StatusBadRequest)
		}

		// Forbid updating root category
		if UUID == uuid.Nil {
			w.WriteHeader(http.StatusForbidden)
			return
		}

		// Forbid using category as a parent to itself
		if category.ParentUUID == UUID {
			w.WriteHeader(http.StatusNotAcceptable)
			return
		}

		category.UUID = UUID
		catalog := store.Catalog{}
		err = catalog.UpdateCategory(db, &category)
		if err != nil {
			log.Println(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
		w.WriteHeader(http.StatusOK)
		json.NewEncoder(w).Encode(category)
	}
}

// DeleteCategory godoc
// @Tags Categories
// @Summary Deletes category
// @Description Deletes a books category based on UUID
// @ID delete-category
// @Produce	json
// @Param uuid path string true "Category UUID"
// @Success 200
// @Failure 400,404 {object} string
// @Failure 500,503 {object} string
// @Failure default {object} string
// @Router /categories/{uuid} [delete]
func (c Catalog) DeleteCategory(db *sql.DB) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		category := models.Category{}
		params := mux.Vars(r)

		UUID, err := uuid.FromString(params["uuid"])
		if err != nil {
			log.Println(err)
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		// Forbid deleting root category
		if UUID == uuid.Nil {
			w.WriteHeader(http.StatusForbidden)
			return
		}

		category.UUID = UUID
		catalog := store.Catalog{}
		err = catalog.DeleteCategory(db, &category)
		if err != nil {
			log.Println(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
		w.WriteHeader(http.StatusOK)
	}
}
