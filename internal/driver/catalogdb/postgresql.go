package catalogdb

import (
	"database/sql"
	"log"

	_ "github.com/jackc/pgx/v4/stdlib"
)

func logFatal(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func ConnectDB(pgUrl string) *sql.DB {
	db, err := sql.Open("pgx", pgUrl)
	logFatal(err)

	err = db.Ping()
	logFatal(err)
	return db
}
