package catalogdb

import (
	"database/sql"

	_ "golang-task6/internal/migrations/catalog"

	_ "github.com/lib/pq"

	"github.com/pressly/goose"
)

func MigrateCatalogDB(connString string) error {
	mdb, err := sql.Open("postgres", connString)
	logFatal(err)
	defer mdb.Close()

	err = mdb.Ping()
	logFatal(err)

	err = goose.Up(mdb, "/var")
	logFatal(err)

	return nil
}
