package ordersdb

import (
	"database/sql"

	_ "golang-task6/internal/migrations/orders"

	_ "github.com/lib/pq"

	"github.com/pressly/goose"
)

func MigrateOrdersDB(connString string) error {
	mdb, err := sql.Open("postgres", connString)
	logFatal(err)
	defer mdb.Close()

	err = mdb.Ping()
	logFatal(err)

	err = goose.Up(mdb, "/var")
	logFatal(err)

	return nil
}
