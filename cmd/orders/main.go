package main

import (
	"context"
	"database/sql"
	"fmt"
	driver "golang-task6/internal/driver/ordersdb"
	"golang-task6/internal/orders"
	"golang-task6/internal/proto"

	"log"
	"net/http"

	_ "golang-task6/docs/orders"

	"github.com/gorilla/mux"
	httpSwagger "github.com/swaggo/http-swagger" // http-swagger middleware
	"google.golang.org/grpc"
)

const pgUrl = "host=localhost dbname=db_booksorders user=db_user password=db_pass port=54320 sslmode=disable"

var db *sql.DB

// @title Orders API
// @version 1.0
// @description API Server for Collecting Orders

// @host localhost:8002
// @BasePath /

func init() {
	err := driver.MigrateOrdersDB(pgUrl)
	if err != nil {
		log.Fatal(err)
	}
}

func main() {
	db = driver.ConnectDB(pgUrl)

	conn, err := grpc.Dial(":8001", grpc.WithInsecure())
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	catalog := proto.NewCatalogClient(conn)

	r := mux.NewRouter()

	// Swagger
	r.PathPrefix("/swagger").Handler(httpSwagger.WrapHandler)

	orders := orders.Orders{}

	ctx := context.Background()

	// Categories
	r.HandleFunc("/categories", orders.GetCategories(ctx, catalog)).Methods("GET")
	r.HandleFunc("/categories/{uuid}", orders.GetCategory(ctx, catalog)).Methods("GET")

	// Authors
	r.HandleFunc("/authors", orders.GetAuthors(ctx, catalog)).Methods("GET")
	r.HandleFunc("/authors/{uuid}", orders.GetAuthor(ctx, catalog)).Methods("GET")

	// Books
	r.HandleFunc("/books", orders.GetBooks(ctx, catalog)).Methods("GET")
	r.HandleFunc("/books/{uuid}", orders.GetBook(ctx, catalog)).Methods("GET")

	// Filters
	r.HandleFunc("/books/author/{uuid}", orders.GetBooksByAuthor(ctx, catalog)).Methods("GET")
	r.HandleFunc("/books/category/{uuid}", orders.GetBooksByCategory(ctx, catalog)).Methods("GET")

	// Orders
	r.HandleFunc("/orders", orders.GetOrders(db)).Methods("GET")
	r.HandleFunc("/orders/{uuid}", orders.GetOrder(db)).Methods("GET")
	r.HandleFunc("/orders", orders.AddOrder(db)).Methods("POST")
	r.HandleFunc("/orders/{uuid}", orders.UpdateOrder(db)).Methods("PUT")
	r.HandleFunc("/orders/{uuid}", orders.DeleteOrder(db)).Methods("DELETE")

	fmt.Println("HTTP server running on port :8002")
	err = http.ListenAndServe(":8002", r)
	if err != nil {
		log.Fatal(err)
	}

}
