package main

import (
	"database/sql"
	"fmt"
	_ "golang-task6/docs/catalog"
	"golang-task6/internal/catalog"
	driver "golang-task6/internal/driver/catalogdb"
	"golang-task6/internal/grpcserver"
	"golang-task6/internal/proto"
	"log"
	"net"
	"net/http"

	"github.com/gorilla/mux"
	httpSwagger "github.com/swaggo/http-swagger" // http-swagger middleware
	"google.golang.org/grpc"
)

const pgUrl = "host=localhost dbname=db_bookscat user=db_user password=db_pass port=54320 sslmode=disable"

var db *sql.DB

// @title Bookstore Catalog API
// @version 1.0
// @description API Server for Bookstore Catalog

// @host localhost:8000
// @BasePath /

func init() {
	err := driver.MigrateCatalogDB(pgUrl)
	if err != nil {
		log.Fatal(err)
	}
}

func main() {
	db = driver.ConnectDB(pgUrl)

	r := mux.NewRouter()

	// Swagger
	r.PathPrefix("/swagger").Handler(httpSwagger.WrapHandler)

	catalog := catalog.Catalog{}

	// Categories
	r.HandleFunc("/categories", catalog.GetCategories(db)).Methods("GET")
	r.HandleFunc("/categories/{uuid}", catalog.GetCategory(db)).Methods("GET")
	r.HandleFunc("/categories", catalog.AddCategory(db)).Methods("POST")
	r.HandleFunc("/categories/{uuid}", catalog.UpdateCategory(db)).Methods("PUT")
	r.HandleFunc("/categories/{uuid}", catalog.DeleteCategory(db)).Methods("DELETE")

	// Authors
	r.HandleFunc("/authors", catalog.GetAuthors(db)).Methods("GET")
	r.HandleFunc("/authors/{uuid}", catalog.GetAuthor(db)).Methods("GET")
	r.HandleFunc("/authors", catalog.AddAuthor(db)).Methods("POST")
	r.HandleFunc("/authors/{uuid}", catalog.UpdateAuthor(db)).Methods("PUT")
	r.HandleFunc("/authors/{uuid}", catalog.DeleteAuthor(db)).Methods("DELETE")

	// Books
	r.HandleFunc("/books", catalog.GetBooks(db)).Methods("GET")
	r.HandleFunc("/books/{uuid}", catalog.GetBook(db)).Methods("GET")
	r.HandleFunc("/books", catalog.AddBook(db)).Methods("POST")
	r.HandleFunc("/books/{uuid}", catalog.UpdateBook(db)).Methods("PUT")
	r.HandleFunc("/books/{uuid}", catalog.DeleteBook(db)).Methods("DELETE")
	r.HandleFunc("/books/{uuid}", catalog.RemoveBookFromCategory(db)).Methods("PATCH")

	// Filters
	r.HandleFunc("/books/category/{uuid}", catalog.GetBooksByCategory(db)).Methods("GET")
	r.HandleFunc("/books/author/{uuid}", catalog.GetBooksByAuthor(db)).Methods("GET")

	go func() {
		gs := grpc.NewServer()
		cs := grpcserver.NewCatalog(db)

		proto.RegisterCatalogServer(gs, cs)

		l, err := net.Listen("tcp", ":8001")
		if err != nil {
			log.Fatalf("failed to listen: %v", err)
		}

		if err := gs.Serve(l); err != nil {
			log.Fatalf("failed to serve: %v", err)
		}

		fmt.Println("gRPC server running on port :8001")
		log.Fatal(gs.Serve(l))
	}()

	fmt.Println("HTTP server running on port :8000")
	err := http.ListenAndServe(":8000", r)
	if err != nil {
		log.Fatal(err)
	}

}
