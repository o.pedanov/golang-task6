Golang Task 6
=

Golang learning - Task 6
-

### Steps for running project:
***
1. Execute docker-compose for PSQL database:
```
sudo docker-compose up -d
```

2. Create Database for Catalog Server
```
sudo docker-compose exec pgdb psql -U db_user -c 'CREATE DATABASE db_bookscat'
```

3. Create Database for Orders Service
```
sudo docker-compose exec pgdb psql -U db_user -c 'CREATE DATABASE db_booksorders'
```

4. Build project
```
make
```

or execute precompiled Server binary
```
./catalog
```

and Client binary
```
./orders
```

5. Open <http://localhost:8000> for managing Catalog Server and <http://localhost:8002> for managing Orders Service
***
_P.S. gRPC Server is running on port 8001_