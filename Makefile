.PHONY: build
build:
		swag init -g ./cmd/catalog/main.go --exclude ./internal/orders -o ./docs/catalog
		swag init -g ./cmd/orders/main.go --exclude ./internal/catalog -o ./docs/orders
		go build -v ./cmd/catalog
		go build -v ./cmd/orders

.PHONY: test
test:
		go test -v -race -timeout 30s ./...

.PHONY: catalog
catalog:
		go run cmd/catalog/main.go

.PHONY: orders
orders:
		go run cmd/orders/main.go

.DEFAULT_GOAL := build